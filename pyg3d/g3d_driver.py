from typing import List

import netCDF4 as nc
import numpy as np


# definition of structures of the G3d file
class RootGrp:

    # Attributes names
    DATASET_TYPE = "dataset_type"
    HISTORY_NAME = "history"
    DATALAYER_COUNT = "datalayer_count"

    # Enum type for dataset_type
    TYPE_FLYTEXTURE = "FlyTexture"

    # Variables
    DATALAYER_DISPLAY_NAMES = "datalayer_display_name"
    DATALAYER_VARIABLES_NAMES = "datalayer_variable_name"

    class DataLayerGrp:
        # Dimension names
        VECTOR_DIM = "vector"
        LENGTH_DIM = "length"
        HEIGHT_DIM = "height"
        POSITION_DIM = "position"

        # Attributes
        VALIDITY_ATTRIBUTE = "is_valid"

        # Variables
        LATITUDE_VARIABLE = "latitude"  # PointCloud only
        LONGITUDE_VARIABLE = "longitude"  # PointCloud only
        ELEVATION = "elevation"  # PointCloud only


def create_g3d_from_texture(
    output_file_name: str,
    latitudes: np.ndarray,
    longitudes: np.ndarray,
    bottom_elevation: np.ndarray,
    top_elevation: np.ndarray,
    data_matrix: List[np.ndarray],
    layer_names: List[str],
    data_variable_names: List[str],
):
    """
    Create a g3d texture along position line
    :param output_file_name: the ouput file path
    :param latitudes: a 1D array of latitudes position
    :param longitudes: a 1D array of longitudes positions
    :param bottom_elevation: a 1D array of bottom points elevation
    :param top_elevation:  a 1D array of top points elevation
    :param data_matrix: a array a data matrix all of them having the same size. Length is the same as latitudes/longitudes.
    :param layer_names: the list of layer names
    :param data_variable_names: the list of data variable name, as written in the g3d file as variables
    :return:
    """

    # Sanity check for size hypothesis

    # retrieve height and length of first data values
    lenght = data_matrix[0].shape[1]
    height = data_matrix[0].shape[0]
    # check assumption for size, supposed to be all the same

    for data in data_matrix:
        lenght_tmp = data.shape[1]
        height_tmp = data.shape[0]
        if lenght_tmp != lenght or height_tmp != height:
            raise Exception(
                "Texture are supposed to have all the same length and height"
            )
    if len(top_elevation) != lenght or len(bottom_elevation) != lenght:
        raise Exception(f"elevation are supposed to be of size {lenght}")
    if len(latitudes) != lenght or len(longitudes) != lenght:
        raise Exception(f"longitudes and latitudes are supposed to be of size {lenght}")

    layer_count = len(data_matrix)
    if layer_count != len(layer_names) or len(data_variable_names) != layer_count:
        raise Exception(
            f"layer_names and data_variable_names should be of the size size than data_matrix {len(data_matrix)}"
        )

    with nc.Dataset(output_file_name, mode="w") as out:
        # declare type
        out.dataset_type = RootGrp.TYPE_FLYTEXTURE
        out.history = "Created by create_g3d_from_texture function"

        out.createDimension(RootGrp.DATALAYER_COUNT, layer_count)
        v = out.createVariable(
            varname=RootGrp.DATALAYER_DISPLAY_NAMES,
            dimensions=(RootGrp.DATALAYER_COUNT),
            datatype=str,
        )

        for index, name in enumerate(data_variable_names):
            v[index] = name

        v = out.createVariable(
            varname=RootGrp.DATALAYER_VARIABLES_NAMES,
            dimensions=(RootGrp.DATALAYER_COUNT),
            datatype=str,
        )
        for index, name in enumerate(data_variable_names):
            v[index] = name

        # create one group
        groupName = "data"
        slice_group = out.createGroup(groupName)
        # group metadata
        slice_group.is_valid = 1
        slice_group.long_name = groupName

        # create texture
        slice_group.createDimension(RootGrp.DataLayerGrp.LENGTH_DIM, lenght)
        slice_group.createDimension(RootGrp.DataLayerGrp.HEIGHT_DIM, height)

        for index, data in enumerate(data_matrix):

            v = slice_group.createVariable(
                varname=data_variable_names[index],
                dimensions=(
                    RootGrp.DataLayerGrp.HEIGHT_DIM,
                    RootGrp.DataLayerGrp.LENGTH_DIM,
                ),
                datatype=float,
                zlib=True,
            )
            v[:] = data[:]

        # create position vector
        slice_group.createDimension(RootGrp.DataLayerGrp.VECTOR_DIM, 2)
        slice_group.createDimension(RootGrp.DataLayerGrp.POSITION_DIM, lenght)

        latitude = slice_group.createVariable(
            varname=RootGrp.DataLayerGrp.LATITUDE_VARIABLE,
            dimensions=(
                RootGrp.DataLayerGrp.VECTOR_DIM,
                RootGrp.DataLayerGrp.POSITION_DIM,
            ),
            datatype="f8",
        )
        latitude[0, :] = latitudes[:]
        latitude[1, :] = latitudes[:]

        longitude = slice_group.createVariable(
            varname=RootGrp.DataLayerGrp.LONGITUDE_VARIABLE,
            dimensions=(
                RootGrp.DataLayerGrp.VECTOR_DIM,
                RootGrp.DataLayerGrp.POSITION_DIM,
            ),
            datatype="f8",
        )
        longitude[0, :] = longitudes[:]
        longitude[1, :] = longitudes[:]

        depth = slice_group.createVariable(
            varname=RootGrp.DataLayerGrp.ELEVATION,
            dimensions=(
                RootGrp.DataLayerGrp.VECTOR_DIM,
                RootGrp.DataLayerGrp.POSITION_DIM,
            ),
            datatype="f4",
        )
        depth[0, :] = top_elevation[:]
        depth[1, :] = bottom_elevation[:]
