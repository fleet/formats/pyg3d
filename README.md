# pyg3d

Python code repository related to G3D format

G3D is a dedicated simple format that allow to easily display georeferenced data textures. For example texture of water column data


## How to use

. Install pyg3d in your environment with PIP

```
pip install pyg3d --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/2808/packages/pypi/simple
```

. Now you can use it
```
import pyg3d
....

```

## Deploy new release

pyG3d project can be pip installable. Use the following commands (with gitlab-pyg3d pointing to package repository):

```
python -m build
python -m twine upload --repository gitlab_pyg3d dist/*
```


Note : id of server is defined in ~/.pypirc.


